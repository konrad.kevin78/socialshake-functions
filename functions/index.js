const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp(functions.config().firebase);

notifyInvitation = (change) => {
    const invitation = change.after.val();
    console.log("AFTER", change.after.val());

    return admin.database().ref(`/users/${invitation.from}`).once("value")
        .then(data => {
            let user = data.val();
            console.log("USER", user);
            let payload = {
                data: {
                    title: `New invitation`,
                    message: `You just matched with someone!`
                },
                token: user.deviceToken
            };

            return admin.messaging().send(payload);
        });
};

createToInvitation = (snapshot, context) => {
    const invitation = snapshot.val();
    console.log("INVIATTION", invitation);

    return admin.database().ref(`/users`).orderByChild("gloveId").equalTo(invitation.gloveIdTo).once('value')
        .then(data => {
            let user = {
                uid : "GjwHLPjpN4cG9bM73X1TSx6vbzf1",
                email : "konrad.kevin78@gmail.com",
                name : "Kevin Konrad",
                gloveId : "1110180745",
                deviceToken : "ft8w9zFYiiE:APA91bHwIT_RF9Ag-3Kl0T4MZVpcTcqf44VTl2S0CkaYlPwjdQqF8k13Igh0c8_vr5W1aunM1wxSMvaZUhd7AGc5rcsvlWROn9i7eI6HoP9V048BXgrX2kK-t6T7F2m-AY8f3U_4uGV2"
            };
            return admin.database().ref(`/invitations/${context.params.invitationId}`).child("to").set(user);
        });
};

createFromInvitation = (snapshot, context) => {
    const invitation = snapshot.val();
    console.log("INVIATTION", invitation);

    return admin.database().ref(`/users`).orderByChild("gloveId").equalTo(invitation.gloveIdFrom).once('value')
        .then(data => {
            let user = {
                uid : "GjwHLPjpN4cG9bM73X1TSx6vbzf1",
                email : "konrad.kevin78@gmail.com",
                name : "Kevin Konrad",
                gloveId : "1110180745",
                deviceToken : "ft8w9zFYiiE:APA91bHwIT_RF9Ag-3Kl0T4MZVpcTcqf44VTl2S0CkaYlPwjdQqF8k13Igh0c8_vr5W1aunM1wxSMvaZUhd7AGc5rcsvlWROn9i7eI6HoP9V048BXgrX2kK-t6T7F2m-AY8f3U_4uGV2"
            };
            return admin.database().ref(`/invitations/${context.params.invitationId}`).child("from").set(user);
        });
};

createTemp = (snapshot, context) => {
    const temp = snapshot.val();

    return admin.database().ref(`/users`).orderByChild("gloveId").equalTo(temp.gloveIdFrom).once('value').then(snapshot1 => {
        const user1 = snapshot1.val();
        console.log("USER1", user1);
        let key1 = Object.keys(user1)[0];
        console.log("USERKEY1", key1);

        return admin.database().ref(`/users`).orderByChild("gloveId").equalTo(temp.gloveIdTo).once('value').then(snapshot2 => {
            const user2 = snapshot2.val();
            console.log("USER2", user2);
            let key2 = Object.keys(user2)[0];
            console.log("USERKEY2", key2);

            const invitation = {
                from : key1,
                to : key2,
                creationDate : new Date().getTime(),
                matchFrom: 1,
                matchTo: 0
            };

            return admin.database().ref(`/invitations/${context.params.tempId}`).set(invitation);
        });
    });
};

exports.createTemp = functions.database.ref(`/temp/{tempId}`).onCreate(createTemp);
exports.createToInvitation = functions.database.ref(`/invitations/{invitationId}`).onCreate(createToInvitation);
exports.createFromInvitation = functions.database.ref(`/invitations/{invitationId}`).onCreate(createFromInvitation);
exports.notifyInvitation = functions.database.ref(`/invitations/{invitationId}`).onUpdate(notifyInvitation);